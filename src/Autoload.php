<?php
/**
 * @file        Autoloads.php
 * @description Composer plugin for processing packages autoloads
 *
 * PHP Version  7.1
 *
 * @package     effect.com/composer-package-autoload
 *
 * @author      Vadim Pshentsov <pshentsoff@gmail.com>
 *
 * @created     19.02.18
 *
 */

namespace Effect\Composer\Package;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Json\JsonFile;
use Composer\Plugin\PluginInterface;
use Composer\Repository\ComposerRepository;
use Composer\Script\Event;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\Script\ScriptEvents;

class Autoload implements PluginInterface, EventSubscriberInterface
{
	/**
	 * @var string Plugin name
	 */
	public static $plugin_name = 'composer-package-autoload';
	/**
	 * @var array supported autoload types
	 */
	public static $types = ['psr-0', 'psr-4', 'classmap', 'exclude-from-classmap'];
	/**
	 * @var Composer
	 */
	protected $composer;
	/**
	 * @var IOInterface
	 */
	protected $io;

	/**
	 * @inheritdoc
	 */
	public function activate( Composer $composer, IOInterface $IO )
	{
		$this->composer = $composer;
		$this->io = $IO;
	}

	/**
	 * @inheritdoc
	 */
	public static function getSubscribedEvents()
	{
		return [
			ScriptEvents::PRE_AUTOLOAD_DUMP => 'onPreAutoloadDump',
		];
	}

	/**
	 * Processing repositories and their settings
	 *
	 * @param Event $event
	 */
	public function onPreAutoloadDump( Event $event )
	{
		$io = $event->getIO();
		$io->write('<info>Processing repositories with '. static::$plugin_name . ' plugin</info>', true, $io::NORMAL);

		$composer = $event->getComposer();
		$root_package = $composer->getPackage();
		$package_autoload = $root_package->getAutoload();
		$repositories = $composer->getRepositoryManager()->getRepositories();

		$Config = $event->getComposer()->getConfig();
		$vendorPath = $Config->get('vendor-dir') . '/';

		foreach ( $repositories as $repository ) {

			/**
			 * [LogicException]
			Composer repositories that have providers can not load the complete list of packages,
			use getProviderNames instead.
			 */
			if ( $repository instanceof ComposerRepository && $repository->hasProviders()) {
				$io->write('Repository has providers - skip', true, $io::VERBOSE);
				continue;
			}

			$packages = $repository->getPackages();

			foreach ( $packages as $package ) {

				$extra = $package->getExtra();

				if ( 'path' === $package->getDistType() && isset( $extra['autoload-plugin'] ) && static::$plugin_name === $extra['autoload-plugin'] ) {

//					$path = $package->getDistUrl();
					$path = $vendorPath . $package->getName();
					$config_file = $path . '/composer.json';

					$io->write('Package <comment>' . $package->getName() . "</comment> dist-type is <info>'path'</info> and <info>{$extra['autoload-plugin']}</info> plugin selected for processing. Checking package autoload.", true, $io::VERBOSE);
					$io->write('Dist URL for <info>' . $package->getName() . '</info> is <info>' . $path . '</info>', true, $io::VERBOSE);

					if ( file_exists( $config_file ) ) {

						$io->write("File <comment>$config_file</comment> found - loading", true, $io::VERBOSE);

						$config = ( new JsonFile( $config_file ) )->read();

						foreach ( static::$types as $type ) {

							$package_autoload[$type] = $package_autoload[$type] ?? [];

							if ( isset( $config['autoload'][$type] ) ) {
								$io->write("<info>Autoload of <comment>{$type}</comment> type found - adding to the root package autoloads.</info>", true, $io::VERBOSE);

								foreach ( $config['autoload'][ $type ] as $key => $path ) {
									$config['autoload'][ $type ][$key] = $vendorPath . $package->getName() . '/' . $path;
								}


								$package_autoload[$type] = array_merge( $package_autoload[$type], $config['autoload'][$type] );
							}

						}

					} else {

						$io->write("File <comment>$config_file</comment> not found.", true, $io::VERBOSE);

					}

				}
			}

		}

		$root_package->setAutoload( $package_autoload );
	}
}
 